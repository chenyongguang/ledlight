  <!DOCTYPE html>
  <html>
    <head>
   		{{template "templates/header.tpl" .}}
      {{template "header" .}}
    </head>
    <body>

          {{template "templates/navbar.tpl" .}}
          {{template "navbar" .}}
   
          {{.LayoutContent}}
    </body>
    <footer>
          {{template "templates/footer.tpl" .}}
          {{template "footer" .}}
    </footer>
  </html>

