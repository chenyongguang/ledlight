package controllers

import (
	"ledlight/utils"

	"github.com/astaxie/beego"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) Get() {
	lights := utils.GetAllLightStatus()

	c.Data["lights"] = lights
	c.Layout = "templates/base.tpl"

	c.TplName = "home.html"
}

func (c *MainController) Post() {
	var light utils.Light
	light.Name = c.GetString("lightName")
	reported := c.GetString("lightReported")
	if reported == "OFF" {
		light.Desired = "ON"
	} else {
		light.Desired = "OFF"
	}
	_ = light.SetLigntStatus()

	lights := utils.GetAllLightStatus()

	c.Data["lights"] = lights
	c.Layout = "templates/base.tpl"

	c.TplName = "home.html"

}
