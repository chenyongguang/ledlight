package utils

import (
	"errors"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/klog"
)

type Light struct {
	Name     string
	Desired  string
	Reported string
}

func GetAllLightStatus() []Light {
	var (
		lights []Light
		light  Light
	)

	devices, _ := GetKubeCRDClientset().Namespace("default").List(metav1.ListOptions{})
	for _, device := range devices.Items {
		klog.Infof("device name: %s\n", device.GetName())
		twins, found, err := unstructured.NestedSlice(device.Object, "status", "twins")
		if err != nil || !found {
			klog.Infof("error=%s", err)
			continue
		}
		light.Name = device.GetName()
		// klog.Infof(" * %s (%v )\n", device.GetName(), twins)
		// for _, twin := range twins {
		desired, found, err := unstructured.NestedString(twins[0].(map[string]interface{}), "desired", "value")
		if err != nil || !found {
			klog.Infof("error=%s", err)
		}
		light.Desired = desired
		// klog.Infof("desired value:  %s", desired)
		reported, found, err := unstructured.NestedString(twins[0].(map[string]interface{}), "reported", "value")
		if err != nil || !found {
			klog.Infof("error=%s", err)

		}
		light.Reported = reported
		// klog.Infof("reported value:  %s", reported)

		// }
	}
	lights = append(lights, light)
	return lights
}

//  SetLigntStatus: set a light status as expect.
func (l *Light) SetLigntStatus() error {
	klog.Infof("Set %s status to  %s", l.Name, l.Desired)

	crdClient := GetKubeCRDClientset()
	device, _ := crdClient.Namespace("default").Get(l.Name, metav1.GetOptions{})
	if device == nil {
		klog.Infof("device %s not found.", l.Name)

		return errors.New("device not found.")
	}
	// klog.Infof("device name: %s\n", device.GetName())
	twins, found, err := unstructured.NestedSlice(device.Object, "status", "twins")
	if err != nil || !found {
		klog.Infof("error=%s", err)
		return err
	}
	if err := unstructured.SetNestedField(twins[0].(map[string]interface{}), l.Desired, "desired", "value"); err != nil {
		panic(err)
	}
	if err := unstructured.SetNestedField(device.Object, twins, "status", "twins"); err != nil {
		panic(err)
	}
	if _, err := crdClient.Namespace("default").Update(device, metav1.UpdateOptions{}); err == nil {
		return err
	}
	return nil
}
