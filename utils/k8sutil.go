package utils

import (
	"os"

	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/klog"
)

var (
	deviceGVR = schema.GroupVersionResource{
		Group:    "devices.kubeedge.io",
		Version:  "v1alpha1",
		Resource: "devices",
	}
)

func buildOutOfClusterConfig() (*rest.Config, error) {
	kubeconfigPath := os.Getenv("KUBECONFIG")
	if kubeconfigPath == "" {
		kubeconfigPath = os.Getenv("HOME") + "/.kube/config"
	}
	return clientcmd.BuildConfigFromFlags("", kubeconfigPath)
}

// GetClient returns a k8s clientset to the request from inside of cluster
func getClient() dynamic.Interface {
	config, err := rest.InClusterConfig()
	if err != nil {
		klog.Fatalf("Can not get kubernetes config: %v", err)
	}

	client, err := dynamic.NewForConfig(config)
	if err != nil {
		klog.Fatalf("Can not create kubernetes client: %v", err)
	}

	return client
}

// getClientOutOfCluster returns a k8s clientset to the request from outside of cluster
func getClientOutOfCluster() dynamic.Interface {
	config, err := buildOutOfClusterConfig()
	if err != nil {
		klog.Fatalf("Can not get kubernetes config: %v", err)
	}

	client, err := dynamic.NewForConfig(config)
	if err != nil {
		klog.Fatalf("Can not create kubernetes client: %v", err)
	}

	return client
}

// GetKubeClientset
func GetKubeCRDClientset() dynamic.NamespaceableResourceInterface {

	var clientset dynamic.Interface
	_, err := rest.InClusterConfig()
	if err != nil {
		clientset = getClientOutOfCluster()
	} else {
		clientset = getClient()
	}
	crdClient := clientset.Resource(deviceGVR)
	return crdClient
}
