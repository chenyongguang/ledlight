### KubeEdge Led light demo

demo build with beego framework. go v12+.

demo list all ledlight CRDs on cluster, switch light ON/OFF through update CRD. 

**build**:  
amd64 `CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a --installsuffix cgo --ldflags="-s" -o /ledlight`

arm: `CGO_ENABLED=0 GOOS=linux GOARCH=arm go build -a --installsuffix cgo --ldflags="-s" -o /ledlight`

**build docker:**

amd64: `docker build . -f Dockerfile -t TAG` 

arm: `docker build . -f Dockerfile-arm -t TAG`

**run**

`KUBECONFIG=pathofk8skubeconfig ./ledlight `

**deploy on k8s**

create RBAC: `kubectl apply -f deploy/rbac.yaml`

create deployment:  `kubectl apply -f deploy/ledlight-deploy[-arm].yaml`

access: http://your-master-node:30999(nodeport)