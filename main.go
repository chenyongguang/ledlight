package main

import (
	_ "ledlight/routers"
	"github.com/astaxie/beego"
)

func main() {
	beego.Run()
}

