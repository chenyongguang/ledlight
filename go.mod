module ledlight

go 1.13

require (
	github.com/astaxie/beego v1.12.0
	github.com/imdario/mergo v0.3.8 // indirect
	github.com/shiena/ansicolor v0.0.0-20151119151921-a422bbe96644 // indirect
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45 // indirect
	golang.org/x/time v0.0.0-20190921001708-c4c64cad1fd0 // indirect
	k8s.io/api v0.0.0-20191016225839-816a9b7df678 // indirect
	k8s.io/apimachinery v0.0.0-20191020214737-6c8691705fc5
	k8s.io/client-go v0.0.0-20190819141724-e14f31a72a77
	k8s.io/klog v1.0.0
	k8s.io/utils v0.0.0-20191010214722-8d271d903fe4 // indirect
)
